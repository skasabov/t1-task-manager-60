package ru.t1.skasabov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractTaskResponse {

    public TaskBindToProjectResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
