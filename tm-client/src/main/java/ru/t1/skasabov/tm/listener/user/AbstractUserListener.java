package ru.t1.skasabov.tm.listener.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.skasabov.tm.api.endpoint.IUserEndpoint;
import ru.t1.skasabov.tm.listener.AbstractListener;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.dto.model.UserDTO;

@Component
@NoArgsConstructor
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
