package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataBase64LoadRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataBase64LoadListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-base64";

    @NotNull
    private static final String DESCRIPTION = "Load data from base64 file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64LoadListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest();
        request.setToken(getToken());
        domainEndpoint.loadDataBase64(request);
    }

}
