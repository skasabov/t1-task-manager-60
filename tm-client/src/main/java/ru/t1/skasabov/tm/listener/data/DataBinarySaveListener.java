package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataBinarySaveRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-bin";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinarySaveListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BINARY SAVE]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest();
        request.setToken(getToken());
        domainEndpoint.saveDataBinary(request);
    }

}
