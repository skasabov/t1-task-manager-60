package ru.t1.skasabov.tm.listener.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.UserChangePasswordRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "change-user-password";

    @NotNull
    private static final String DESCRIPTION = "Change password of current user.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(password);
        request.setToken(getToken());
        userEndpoint.changeUserPassword(request);
    }

}
