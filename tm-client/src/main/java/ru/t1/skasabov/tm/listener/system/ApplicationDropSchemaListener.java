package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationDropSchemaRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.util.TerminalUtil;

@Component
@NoArgsConstructor
public final class ApplicationDropSchemaListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "drop-schema";

    @NotNull
    private static final String DESCRIPTION = "Drop database schema.";

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationDropSchemaListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DROP SCHEMA]");
        System.out.println("ENTER PASSWORD:");
        @Nullable final String password = TerminalUtil.nextLine();
        @NotNull final ApplicationDropSchemaRequest request = new ApplicationDropSchemaRequest(password);
        systemEndpoint.dropSchema(request);
    }

}
