package ru.t1.skasabov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.event.ConsoleEvent;

public interface IListener {

    @Nullable
    String argument();

    @Nullable
    String description();

    @Nullable
    String command();

    void handler(@NotNull ConsoleEvent consoleEvent);

}
