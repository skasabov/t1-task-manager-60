package ru.t1.skasabov.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.api.service.*;
import ru.t1.skasabov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.skasabov.tm.exception.system.CommandNotSupportedException;
import ru.t1.skasabov.tm.listener.AbstractListener;
import ru.t1.skasabov.tm.event.ConsoleEvent;
import ru.t1.skasabov.tm.exception.system.ArgumentEmptyException;
import ru.t1.skasabov.tm.exception.system.CommandEmptyException;
import ru.t1.skasabov.tm.util.SystemUtil;
import ru.t1.skasabov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final List<String> arguments = new ArrayList<>();

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "tm-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public static void close() {
        System.exit(0);
    }

    public void start(@Nullable final String[] args) {
        initCommands(listeners);
        if (processArguments(args)) close();
        processCommands();
    }

    private void initCommands(@Nullable final AbstractListener[] listeners) {
        for (@Nullable final AbstractListener listener : listeners) {
            if (listener == null) continue;
            commands.add(listener.command());
            arguments.add(listener.argument());
        }
    }

    private void processCommands() {
        prepareStartup();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @Nullable final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
            loggerService.argument(argument);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
        }
        return true;
    }

    public void processCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) throw new CommandEmptyException();
        if (!commands.contains(command)) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void processArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentEmptyException();
        if (!arguments.contains(argument)) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(new ConsoleEvent(argument));
    }

}
