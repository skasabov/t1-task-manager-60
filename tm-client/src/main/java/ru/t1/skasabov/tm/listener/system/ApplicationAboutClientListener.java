package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationAboutClientListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "about-client";

    @NotNull
    private static final String DESCRIPTION = "Show about client program.";

    @NotNull
    private static final String ARGUMENT = "-ac";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationAboutClientListener.command() == #consoleEvent.name " +
            "or @applicationAboutClientListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ABOUT]");
        System.out.println("name: " + getPropertyService().getAuthorName());
        System.out.println("email: " + getPropertyService().getAuthorEmail());
    }

}
