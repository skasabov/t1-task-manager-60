package ru.t1.skasabov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.DataBinaryLoadRequest;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
public final class DataBinaryLoadListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-load-bin";

    @NotNull
    private static final String DESCRIPTION = "Load data from binary file.";

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBinaryLoadListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BINARY LOAD]");
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest();
        request.setToken(getToken());
        domainEndpoint.loadDataBinary(request);
    }

}
