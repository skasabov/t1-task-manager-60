package ru.t1.skasabov.tm.configuration;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.jms.*;

@Configuration
@ComponentScan("ru.t1.skasabov.tm")
public class LoggerConfiguration {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @Bean
    @SneakyThrows
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustAllPackages(true);
        return connectionFactory;
    }

}
