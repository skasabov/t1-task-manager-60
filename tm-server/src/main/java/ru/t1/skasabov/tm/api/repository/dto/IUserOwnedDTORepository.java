package ru.t1.skasabov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

    @NotNull
    Boolean existsById(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @Nullable
    M findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @NotNull Integer index);

    long getSize(@NotNull String userId);

    void removeOneById(@NotNull String userId, @NotNull String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeAll(@NotNull String userId);

    @NotNull
    List<M> findAllByUsers(@NotNull Collection<UserDTO> collection);

}
