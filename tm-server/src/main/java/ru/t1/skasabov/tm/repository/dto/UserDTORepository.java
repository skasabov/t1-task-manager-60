package ru.t1.skasabov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import java.util.List;

@Repository
@NoArgsConstructor
public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager.createQuery("SELECT u FROM UserDTO u", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT u FROM UserDTO u", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(u) FROM UserDTO u", Long.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    @Transactional
    public void removeAll() {
        @NotNull final List<UserDTO> users = findAll();
        for (@NotNull final UserDTO user : users) {
            entityManager.remove(user);
        }
    }

    @Override
    @Transactional
    public void removeOne(@NotNull final UserDTO model) {
        @NotNull final UserDTO user = entityManager.find(UserDTO.class, model.getId());
        entityManager.remove(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@NotNull final String email) {
        return entityManager.createQuery("SELECT u FROM UserDTO u WHERE u.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }
    
}
