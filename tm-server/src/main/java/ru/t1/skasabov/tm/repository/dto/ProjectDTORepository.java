package ru.t1.skasabov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@NoArgsConstructor
public class ProjectDTORepository extends AbstractUserOwnedDTORepository<ProjectDTO> implements IProjectDTORepository {

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p", ProjectDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p", ProjectDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    @Transactional
    public void removeAll() {
        @NotNull final List<ProjectDTO> projects = findAll();
        for (@NotNull final ProjectDTO project : projects) {
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(p) FROM ProjectDTO p WHERE p.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    @Transactional
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    @Transactional
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    @Transactional
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<ProjectDTO> projects = findAll(userId);
        for (@NotNull final ProjectDTO project : projects) {
            entityManager.remove(project);
        }
    }

    @Override
    @Transactional
    public void removeOne(@NotNull final ProjectDTO model) {
        @NotNull final ProjectDTO project = entityManager.find(ProjectDTO.class, model.getId());
        entityManager.remove(project);
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByCreated() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.created",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByStatus() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.status",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByName() {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p ORDER BY p.name",
                        ProjectDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.created",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.status",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p.name",
                        ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAllByUsers(@NotNull final Collection<UserDTO> collection) {
        if (collection.isEmpty()) return Collections.emptyList();
        return entityManager.createQuery("SELECT p FROM ProjectDTO p INNER JOIN FETCH UserDTO u ON p.userId = u.id WHERE u.id IN :users",
                        ProjectDTO.class)
                .setParameter("users", collection.stream().map(AbstractModelDTO::getId).collect(Collectors.toList()))
                .getResultList();
    }
    
}
