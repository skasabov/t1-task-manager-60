package ru.t1.skasabov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.dto.IUserDTOService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.exception.user.ExistsEmailException;
import ru.t1.skasabov.tm.exception.user.ExistsLoginException;
import ru.t1.skasabov.tm.exception.user.PasswordHashEmptyException;
import ru.t1.skasabov.tm.exception.field.RoleEmptyException;
import ru.t1.skasabov.tm.util.HashUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public class UserDTOService extends AbstractDTOService<UserDTO> implements IUserDTOService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDTORepository repository;

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private ISessionDTORepository sessionRepository;

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final Collection<UserDTO> collection) {
        if (collection == null) return;
        @NotNull final List<TaskDTO> tasks = taskRepository.findAllByUsers(collection);
        @NotNull final List<SessionDTO> sessions = sessionRepository.findAllByUsers(collection);
        @NotNull final List<ProjectDTO> projects = projectRepository.findAllByUsers(collection);
        taskRepository.removeAll(tasks);
        sessionRepository.removeAll(sessions);
        @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjects(projects);
        taskRepository.removeAll(projectTasks);
        projectRepository.removeAll(projects);
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<UserDTO> addAll(@Nullable final Collection<UserDTO> models) {
        if (models == null) return Collections.emptyList();
        repository.addAll(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<UserDTO> set(@Nullable final Collection<UserDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final List<TaskDTO> tasks = taskRepository.findAll();
        @NotNull final List<SessionDTO> sessions = sessionRepository.findAll();
        @NotNull final List<ProjectDTO> projects = projectRepository.findAll();
        taskRepository.removeAll(tasks);
        sessionRepository.removeAll(sessions);
        projectRepository.removeAll(projects);
        repository.set(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO add(@Nullable final UserDTO model) {
        if (model == null) throw new ModelEmptyException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeOne(@Nullable final UserDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final List<TaskDTO> tasks = taskRepository.findAll(model.getId());
        @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(model.getId());
        @NotNull final List<ProjectDTO> projects = projectRepository.findAll(model.getId());
        taskRepository.removeAll(tasks);
        sessionRepository.removeAll(sessions);
        @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjects(projects);
        taskRepository.removeAll(projectTasks);
        projectRepository.removeAll(projects);
        repository.removeOne(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO model = findOneById(id);
        if (model == null) return null;
        @NotNull final List<TaskDTO> tasks = taskRepository.findAll(id);
        @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(id);
        @NotNull final List<ProjectDTO> projects = projectRepository.findAll(id);
        taskRepository.removeAll(tasks);
        sessionRepository.removeAll(sessions);
        @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjects(projects);
        taskRepository.removeAll(projectTasks);
        projectRepository.removeAll(projects);
        repository.removeOneById(id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final UserDTO model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final List<TaskDTO> tasks = taskRepository.findAll(model.getId());
        @NotNull final List<SessionDTO> sessions = sessionRepository.findAll(model.getId());
        @NotNull final List<ProjectDTO> projects = projectRepository.findAll(model.getId());
        taskRepository.removeAll(tasks);
        sessionRepository.removeAll(sessions);
        @NotNull final List<TaskDTO> projectTasks = taskRepository.findAllByProjects(projects);
        taskRepository.removeAll(projectTasks);
        projectRepository.removeAll(projects);
        repository.removeOneByIndex(index);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() {
        @NotNull final List<TaskDTO> tasks = taskRepository.findAll();
        @NotNull final List<SessionDTO> sessions = sessionRepository.findAll();
        @NotNull final List<ProjectDTO> projects = projectRepository.findAll();
        taskRepository.removeAll(tasks);
        sessionRepository.removeAll(sessions);
        projectRepository.removeAll(projects);
        repository.removeAll();
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new ExistsEmailException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        repository.add(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordHashEmptyException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        repository.add(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDTO user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return removeOne(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return repository.isLoginExist(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return repository.isEmailExist(email);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
        return user;
    }

}
