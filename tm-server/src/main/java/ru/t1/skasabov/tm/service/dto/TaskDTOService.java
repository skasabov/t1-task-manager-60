package ru.t1.skasabov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.service.dto.ITaskDTOService;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.TaskNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;

import java.util.*;

@Service
@NoArgsConstructor
public class TaskDTOService extends AbstractUserOwnedDTOService<TaskDTO> implements ITaskDTOService {

    @NotNull
    @Autowired
    private ITaskDTORepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final Collection<TaskDTO> collection) {
        if (collection == null) return;
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<TaskDTO> addAll(@Nullable final Collection<TaskDTO> models) {
        if (models == null) return Collections.emptyList();
        repository.addAll(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<TaskDTO> set(@Nullable final Collection<TaskDTO> models) {
        if (models == null) return Collections.emptyList();
        repository.set(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@Nullable final TaskDTO model) {
        if (model == null) throw new ModelEmptyException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final Sort sortType) {
        if (sortType == null) return findAll();
        @NotNull List<TaskDTO> tasks;
        switch(sortType) {
            case BY_CREATED:
                tasks = repository.findAllSortByCreated();
                break;
            case BY_STATUS:
                tasks = repository.findAllSortByStatus();
                break;
            case BY_NAME:
                tasks = repository.findAllSortByName();
                break;
            default:
                tasks = Collections.emptyList();
                break;
        }
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOne(@Nullable final TaskDTO model) {
        if (model == null) throw new ModelEmptyException();
        repository.removeOne(model);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(id);
        if (model == null) return null;
        repository.removeOneById(id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final TaskDTO model = findOneByIndex(index);
        if (model == null) return null;
        repository.removeOneByIndex(index);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(@Nullable final String userId, @Nullable final Sort sortType) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sortType == null) return findAll(userId);
        @NotNull List<TaskDTO> tasks;
        switch(sortType) {
            case BY_CREATED:
                tasks = repository.findAllSortByCreatedForUser(userId);
                break;
            case BY_STATUS:
                tasks = repository.findAllSortByStatusForUser(userId);
                break;
            case BY_NAME:
                tasks = repository.findAllSortByNameForUser(userId);
                break;
            default:
                tasks = Collections.emptyList();
                break;
        }
        return tasks;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO model = findOneById(userId, id);
        if (model == null) return null;
        repository.removeOneById(userId, id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO model = findOneByIndex(userId, index);
        if (model == null) return null;
        repository.removeOneByIndex(userId, index);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        if (dateBegin == null || dateEnd == null) return create(userId, name, description);
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        repository.add(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        repository.update(task);
        return task;
    }

}
