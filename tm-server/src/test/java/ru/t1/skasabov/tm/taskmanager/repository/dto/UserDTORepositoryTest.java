package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.SessionDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;
import ru.t1.skasabov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserDTORepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private UserDTO cat;

    @NotNull
    private UserDTO mouse;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private IProjectDTORepository projectRepository;

    @NotNull
    private ITaskDTORepository taskRepository;

    @NotNull
    private ISessionDTORepository sessionRepository;

    @NotNull
    private IUserDTORepository userRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        propertyService = context.getBean(IPropertyService.class);
        userRepository = context.getBean(IUserDTORepository.class);
        cat = new UserDTO();
        cat.setLogin("cat");
        @Nullable final String passwordHashCat = HashUtil.salt(propertyService, "cat");
        Assert.assertNotNull(passwordHashCat);
        cat.setPasswordHash(passwordHashCat);
        cat.setEmail("cat@cat");
        mouse = new UserDTO();
        mouse.setLogin("mouse");
        @Nullable final String passwordHashMouse = HashUtil.salt(propertyService, "mouse");
        Assert.assertNotNull(passwordHashMouse);
        mouse.setPasswordHash(passwordHashMouse);
        mouse.setEmail("mouse@mouse");
        userRepository.add(cat);
        userRepository.add(mouse);
        projectRepository = context.getBean(IProjectDTORepository.class);
        @NotNull final ProjectDTO catProject = new ProjectDTO();
        catProject.setName("Cat Project");
        catProject.setUserId(cat.getId());
        @NotNull final ProjectDTO mouseProject = new ProjectDTO();
        mouseProject.setName("Mouse Project");
        mouseProject.setUserId(mouse.getId());
        projectRepository.add(catProject);
        projectRepository.add(mouseProject);
        taskRepository = context.getBean(ITaskDTORepository.class);
        @NotNull final TaskDTO catTask = new TaskDTO();
        catTask.setName("Cat Task");
        catTask.setUserId(cat.getId());
        @NotNull final TaskDTO mouseTask = new TaskDTO();
        mouseTask.setName("Mouse Task");
        mouseTask.setUserId(mouse.getId());
        taskRepository.add(catTask);
        taskRepository.add(mouseTask);
        sessionRepository = context.getBean(ISessionDTORepository.class);
        @NotNull final SessionDTO catSession = new SessionDTO();
        catSession.setUserId(cat.getId());
        @NotNull final SessionDTO mouseSession = new SessionDTO();
        mouseSession.setUserId(mouse.getId());
        sessionRepository.add(catSession);
        sessionRepository.add(mouseSession);
    }

    @Test
    public void testUpdate() {
        mouse.setLastName("mouse");
        mouse.setFirstName("mouse");
        mouse.setMiddleName("mouse");
        userRepository.update(mouse);
        @Nullable final UserDTO actualUser = userRepository.findByLogin("mouse");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals("mouse", actualUser.getLastName());
        Assert.assertEquals("mouse", actualUser.getFirstName());
        Assert.assertEquals("mouse", actualUser.getMiddleName());
    }

    @Test
    public void testAdd() {
        final long expectedUsers = userRepository.getSize() + 1;
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("dog");
        @Nullable final String passwordHashDog = HashUtil.salt(propertyService, "dog");
        Assert.assertNotNull(passwordHashDog);
        user.setPasswordHash(passwordHashDog);
        userRepository.add(user);
        Assert.assertEquals(expectedUsers, userRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = userRepository.getSize() + 4;
        @NotNull final List<UserDTO> actualUsers = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("user " + i);
            @Nullable final String passwordHashUser = HashUtil.salt(propertyService, "user " + i);
            Assert.assertNotNull(passwordHashUser);
            user.setPasswordHash(passwordHashUser);
            actualUsers.add(user);
        }
        userRepository.addAll(actualUsers);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<UserDTO> actualUsers = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final UserDTO user = new UserDTO();
            user.setLogin("user " + i);
            @Nullable final String passwordHashUser = HashUtil.salt(propertyService, "user " + i);
            Assert.assertNotNull(passwordHashUser);
            user.setPasswordHash(passwordHashUser);
            actualUsers.add(user);
        }
        taskRepository.removeAll();
        sessionRepository.removeAll();
        projectRepository.removeAll();
        userRepository.set(actualUsers);
        Assert.assertEquals(0, projectRepository.getSize());
        Assert.assertEquals(0, taskRepository.getSize());
        Assert.assertEquals(0, sessionRepository.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    public void testClearAll() {
        taskRepository.removeAll();
        sessionRepository.removeAll();
        projectRepository.removeAll();
        userRepository.removeAll();
        Assert.assertEquals(0, projectRepository.getSize());
        Assert.assertEquals(0, taskRepository.getSize());
        Assert.assertEquals(0, sessionRepository.getSize());
        Assert.assertEquals(0, userRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = userRepository.getSize() - 2;
        final long expectedNumberOfTasks = taskRepository.getSize() - 2;
        final long expectedNumberOfProjects = projectRepository.getSize() - 2;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 2;
        @NotNull final List<UserDTO> userList = new ArrayList<>();
        userList.add(cat);
        userList.add(mouse);
        taskRepository.removeAll(cat.getId());
        sessionRepository.removeAll(cat.getId());
        projectRepository.removeAll(cat.getId());
        taskRepository.removeAll(mouse.getId());
        sessionRepository.removeAll(mouse.getId());
        projectRepository.removeAll(mouse.getId());
        userRepository.removeAll(userList);
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<UserDTO> userList = userRepository.findAll();
        Assert.assertEquals(userList.size(), userRepository.getSize());
    }

    @Test
    public void testFindById() {
        @Nullable final UserDTO actualUser = userRepository.findOneById(cat.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIdUserNotFound() {
        Assert.assertNull(userRepository.findOneById("some_id"));
    }

    @Test
    public void testFindByLogin() {
        @Nullable final UserDTO actualUser = userRepository.findByLogin("cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByLoginUserNotFound() {
        Assert.assertNull(userRepository.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        @Nullable final UserDTO actualUser = userRepository.findByEmail("cat@cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByEmailUserNotFound() {
        Assert.assertNull(userRepository.findByEmail("dog@dog"));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final UserDTO user = userRepository.findAll().get(0);
        @Nullable final UserDTO actualUser = userRepository.findOneByIndex(0);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getLogin(), actualUser.getLogin());
        Assert.assertEquals(user.getEmail(), actualUser.getEmail());
        Assert.assertEquals(user.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIndexUserNotFound() {
        Assert.assertNull(userRepository.findOneByIndex(2));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("dog");
        @Nullable final String passwordHashDog = HashUtil.salt(propertyService, "dog");
        Assert.assertNotNull(passwordHashDog);
        user.setPasswordHash(passwordHashDog);
        user.setEmail("dog@dog");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = userRepository.findAll().get(0).getId();
        @NotNull final String invalidId = "some_id";
        Assert.assertFalse(userRepository.existsById(invalidId));
        Assert.assertTrue(userRepository.existsById(validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = userRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        final long expectedNumberOfProjects = projectRepository.getSize() - 1;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 1;
        taskRepository.removeAll(mouse.getId());
        sessionRepository.removeAll(mouse.getId());
        projectRepository.removeAll(mouse.getId());
        userRepository.removeOne(mouse);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = userRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        final long expectedNumberOfProjects = projectRepository.getSize() - 1;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 1;
        taskRepository.removeAll(mouse.getId());
        sessionRepository.removeAll(mouse.getId());
        projectRepository.removeAll(mouse.getId());
        userRepository.removeOneById(mouse.getId());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = userRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        final long expectedNumberOfProjects = projectRepository.getSize() - 1;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 1;
        @NotNull final String userId = userRepository.findAll().get(0).getId();
        taskRepository.removeAll(userId);
        sessionRepository.removeAll(userId);
        projectRepository.removeAll(userId);
        userRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testLoginExist() {
        Assert.assertTrue(userRepository.isLoginExist("cat"));
    }

    @Test
    public void testEmptyLoginExist() {
        Assert.assertFalse(userRepository.isLoginExist(""));
    }

    @Test
    public void testEmailExist() {
        Assert.assertTrue(userRepository.isEmailExist("cat@cat"));
    }

    @Test
    public void testEmptyEmailExist() {
        Assert.assertFalse(userRepository.isEmailExist(""));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        taskRepository.removeAll();
        sessionRepository.removeAll();
        projectRepository.removeAll();
        userRepository.removeAll();
    }

}
