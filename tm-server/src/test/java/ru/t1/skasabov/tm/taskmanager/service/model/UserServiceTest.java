package ru.t1.skasabov.tm.taskmanager.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.service.IPropertyService;
import ru.t1.skasabov.tm.api.service.model.IProjectService;
import ru.t1.skasabov.tm.api.service.model.ISessionService;
import ru.t1.skasabov.tm.api.service.model.ITaskService;
import ru.t1.skasabov.tm.api.service.model.IUserService;
import ru.t1.skasabov.tm.enumerated.Role;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.exception.user.ExistsEmailException;
import ru.t1.skasabov.tm.exception.user.ExistsLoginException;
import ru.t1.skasabov.tm.exception.field.RoleEmptyException;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;
import ru.t1.skasabov.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User cat;

    @NotNull
    private User mouse;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private ITaskService taskService;

    @Before
    public void initTest() {
        propertyService = context.getBean(IPropertyService.class);
        userService = context.getBean(IUserService.class);
        projectService = context.getBean(IProjectService.class);
        sessionService = context.getBean(ISessionService.class);
        taskService = context.getBean(ITaskService.class);
        @NotNull final List<Task> catTaskList = new ArrayList<>();
        @NotNull final List<Project> catProjectList = new ArrayList<>();
        @NotNull final List<Session> catSessionList = new ArrayList<>();
        @NotNull final List<Task> mouseTaskList = new ArrayList<>();
        @NotNull final List<Project> mouseProjectList = new ArrayList<>();
        @NotNull final List<Session> mouseSessionList = new ArrayList<>();

        cat = new User();
        cat.setLogin("cat");
        @Nullable final String passwordHashCat = HashUtil.salt(propertyService, "cat");
        Assert.assertNotNull(passwordHashCat);
        cat.setPasswordHash(passwordHashCat);
        cat.setEmail("cat@cat");
        @NotNull final Project catProject = new Project();
        catProject.setName("cat");
        catProject.setUser(cat);
        @NotNull final Task catTask = new Task();
        catTask.setName("cat");
        catTask.setUser(cat);
        @NotNull final Session catSession = new Session();
        catSession.setUser(cat);
        catProjectList.add(catProject);
        catSessionList.add(catSession);
        catTaskList.add(catTask);
        cat.setProjects(catProjectList);
        cat.setTasks(catTaskList);
        cat.setSessions(catSessionList);

        mouse = new User();
        mouse.setLogin("mouse");
        @Nullable final String passwordHashMouse = HashUtil.salt(propertyService, "mouse");
        Assert.assertNotNull(passwordHashMouse);
        mouse.setPasswordHash(passwordHashMouse);
        mouse.setEmail("mouse@mouse");
        @NotNull final Project mouseProject = new Project();
        mouseProject.setName("mouse");
        mouseProject.setUser(mouse);
        @NotNull final Task mouseTask = new Task();
        mouseTask.setName("mouse");
        mouseTask.setUser(mouse);
        @NotNull final Session mouseSession = new Session();
        mouseSession.setUser(mouse);
        mouseProjectList.add(mouseProject);
        mouseSessionList.add(mouseSession);
        mouseTaskList.add(mouseTask);
        mouse.setProjects(mouseProjectList);
        mouse.setTasks(mouseTaskList);
        mouse.setSessions(mouseSessionList);

        userService.add(cat);
        userService.add(mouse);
        userService.lockUserByLogin("mouse");
    }

    @Test
    public void testAdd() {
        final long expectedUsers = userService.getSize() + 1;
        final long expectedTasks = taskService.getSize() + 2;
        final long expectedProjects = projectService.getSize() + 1;
        final long expectedSessions = sessionService.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        @Nullable final String passwordHashDog = HashUtil.salt(propertyService, "dog");
        Assert.assertNotNull(passwordHashDog);
        user.setPasswordHash(passwordHashDog);
        @NotNull final Project userProject = new Project();
        userProject.setName("dog");
        userProject.setUser(user);
        @NotNull final Task userTask = new Task();
        userTask.setName("dog");
        userTask.setUser(user);
        @NotNull final Session userSession = new Session();
        userSession.setUser(user);
        @NotNull final Task task = new Task();
        task.setName("task");
        task.setProject(userProject);
        task.setUser(user);
        @NotNull final List<Task> projectTasks = new ArrayList<>();
        projectTasks.add(task);
        userProject.setTasks(projectTasks);
        @NotNull final List<Task> userTaskList = new ArrayList<>();
        @NotNull final List<Project> userProjectList = new ArrayList<>();
        @NotNull final List<Session> userSessionList = new ArrayList<>();
        userProjectList.add(userProject);
        userSessionList.add(userSession);
        userTaskList.add(userTask);
        userTaskList.add(task);
        user.setProjects(userProjectList);
        user.setTasks(userTaskList);
        user.setSessions(userSessionList);
        userService.add(user);
        Assert.assertEquals(expectedUsers, userService.getSize());
        Assert.assertEquals(expectedTasks, taskService.getSize());
        Assert.assertEquals(expectedProjects, projectService.getSize());
        Assert.assertEquals(expectedSessions, sessionService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddNull() {
        userService.add(null);
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = userService.getSize() + 4;
        final long expectedNumberOfTasks = taskService.getSize() + 8;
        final long expectedNumberOfProjects = projectService.getSize() + 4;
        final long expectedNumberOfSessions = sessionService.getSize() + 4;
        @NotNull final List<User> actualUsers = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            @Nullable final String passwordHashUser = HashUtil.salt(propertyService, "user " + i);
            Assert.assertNotNull(passwordHashUser);
            user.setPasswordHash(passwordHashUser);
            @NotNull final Project userProject = new Project();
            userProject.setName("user " + i);
            userProject.setUser(user);
            @NotNull final Task userTask = new Task();
            userTask.setName("user " + i);
            userTask.setUser(user);
            @NotNull final Session userSession = new Session();
            userSession.setUser(user);
            @NotNull final Task task = new Task();
            task.setName("task");
            task.setProject(userProject);
            task.setUser(user);
            @NotNull final List<Task> projectTasks = new ArrayList<>();
            projectTasks.add(task);
            userProject.setTasks(projectTasks);
            @NotNull final List<Task> userTaskList = new ArrayList<>();
            @NotNull final List<Project> userProjectList = new ArrayList<>();
            @NotNull final List<Session> userSessionList = new ArrayList<>();
            userProjectList.add(userProject);
            userSessionList.add(userSession);
            userTaskList.add(userTask);
            userTaskList.add(task);
            user.setProjects(userProjectList);
            user.setTasks(userTaskList);
            user.setSessions(userSessionList);
            actualUsers.add(user);
        }
        userService.addAll(actualUsers);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
    }

    @Test
    public void testAddAllNull() {
        final long expectedNumberOfEntries = userService.getSize();
        userService.addAll(null);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<User> actualUsers = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            @Nullable final String passwordHashUser = HashUtil.salt(propertyService, "user " + i);
            Assert.assertNotNull(passwordHashUser);
            user.setPasswordHash(passwordHashUser);
            @NotNull final Project userProject = new Project();
            userProject.setName("user " + i);
            userProject.setUser(user);
            @NotNull final Task userTask = new Task();
            userTask.setName("user " + i);
            userTask.setUser(user);
            @NotNull final Session userSession = new Session();
            userSession.setUser(user);
            @NotNull final Task task = new Task();
            task.setName("task");
            task.setProject(userProject);
            task.setUser(user);
            @NotNull final List<Task> projectTasks = new ArrayList<>();
            projectTasks.add(task);
            userProject.setTasks(projectTasks);
            @NotNull final List<Task> userTaskList = new ArrayList<>();
            @NotNull final List<Project> userProjectList = new ArrayList<>();
            @NotNull final List<Session> userSessionList = new ArrayList<>();
            userProjectList.add(userProject);
            userSessionList.add(userSession);
            userTaskList.add(userTask);
            userTaskList.add(task);
            user.setProjects(userProjectList);
            user.setTasks(userTaskList);
            user.setSessions(userSessionList);
            actualUsers.add(user);
        }
        userService.set(actualUsers);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userService.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectService.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES * 2, taskService.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionService.getSize());
    }

    @Test
    public void testSetNull() {
        final long expectedNumberOfEntries = userService.getSize();
        userService.set(null);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testCreate() {
        final long expectedNumberOfEntries = userService.getSize() + 1;
        userService.create("dog", "dog", "dog@dog");
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testCreateEmptyLogin() {
        userService.create("", "dog", "dog@dog");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testCreateEmptyPassword() {
        userService.create("dog", "", "dog@dog");
    }

    @Test(expected = EmailEmptyException.class)
    public void testCreateEmptyEmail() {
        userService.create("dog", "dog", "");
    }

    @Test(expected = ExistsLoginException.class)
    public void testCreateLoginExists() {
        userService.create("cat", "cat", "cat@cat");
    }

    @Test(expected = ExistsEmailException.class)
    public void testCreateEmailExists() {
        userService.create("dog", "dog", "cat@cat");
    }

    @Test
    public void testCreateRole() {
        final long expectedNumberOfEntries = userService.getSize() + 1;
        userService.create("dog", "dog", Role.USUAL);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test(expected = RoleEmptyException.class)
    public void testCreateRoleNull() {
        userService.create("dog", "dog", (Role) null);
    }

    @Test
    public void testClearAll() {
        userService.removeAll();
        Assert.assertEquals(0, userService.getSize());
        Assert.assertEquals(0, projectService.getSize());
        Assert.assertEquals(0, sessionService.getSize());
        Assert.assertEquals(0, taskService.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = userService.getSize() - 2;
        final long expectedNumberOfTasks = taskService.getSize() - 2;
        final long expectedNumberOfProjects = projectService.getSize() - 2;
        final long expectedNumberOfSessions = sessionService.getSize() - 2;
        @NotNull final List<User> userList = new ArrayList<>();
        userList.add(cat);
        userList.add(mouse);
        userService.removeAll(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> userList = userService.findAll();
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testFindById() {
        @Nullable final User actualUser = userService.findOneById(cat.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test(expected = IdEmptyException.class)
    public void testFindByEmptyId() {
        Assert.assertNull(userService.findOneById(""));
    }

    @Test
    public void testFindByIdUserNotFound() {
        Assert.assertNull(userService.findOneById("some_id"));
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User actualUser = userService.findByLogin("cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void testFindByEmptyLogin() {
        Assert.assertNull(userService.findByLogin(""));
    }

    @Test
    public void testFindByLoginUserNotFound() {
        Assert.assertNull(userService.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User actualUser = userService.findByEmail("cat@cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test(expected = EmailEmptyException.class)
    public void testFindByEmptyEmail() {
        Assert.assertNull(userService.findByEmail(""));
    }

    @Test
    public void testFindByEmailUserNotFound() {
        Assert.assertNull(userService.findByEmail("dog@dog"));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final User user = userService.findAll().get(0);
        @Nullable final User actualUser = userService.findOneByIndex(0);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getLogin(), actualUser.getLogin());
        Assert.assertEquals(user.getEmail(), actualUser.getEmail());
        Assert.assertEquals(user.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), actualUser.getRole());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByEmptyIndex() {
        userService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByNegativeIndex() {
        userService.findOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testFindByIncorrectIndex() {
        userService.findOneByIndex((int) userService.getSize() + 1);
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = userService.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        @Nullable final String passwordHashDog = HashUtil.salt(propertyService, "dog");
        Assert.assertNotNull(passwordHashDog);
        user.setPasswordHash(passwordHashDog);
        user.setEmail("dog@dog");
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = userService.findAll().get(0).getId();
        @NotNull final String invalidId = "some_id";
        Assert.assertFalse(userService.existsById(invalidId));
        Assert.assertTrue(userService.existsById(validId));
    }

    @Test(expected = IdEmptyException.class)
    public void testIsNotFoundByEmptyId() {
        userService.existsById("");
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = userService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        final long expectedNumberOfProjects = projectService.getSize() - 1;
        final long expectedNumberOfSessions = sessionService.getSize() - 1;
        userService.removeOne(mouse);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = ModelEmptyException.class)
    public void testRemoveNull() {
        userService.removeOne(null);
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = userService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        final long expectedNumberOfProjects = projectService.getSize() - 1;
        final long expectedNumberOfSessions = sessionService.getSize() - 1;
        userService.removeOneById(mouse.getId());
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = IdEmptyException.class)
    public void testRemoveByEmptyId() {
        Assert.assertNull(userService.removeOneById(""));
    }

    @Test
    public void testRemoveByIdUserNotFound() {
        Assert.assertNull(userService.removeOneById("some_id"));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = userService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        final long expectedNumberOfProjects = projectService.getSize() - 1;
        final long expectedNumberOfSessions = sessionService.getSize() - 1;
        userService.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByNegativeIndex() {
        userService.removeOneByIndex(-2);
    }

    @Test(expected = IndexIncorrectException.class)
    public void testRemoveByIncorrectIndex() {
        userService.removeOneByIndex((int) userService.getSize() + 1);
    }

    @Test
    public void testRemoveByLogin() {
        final long expectedNumberOfEntries = userService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        final long expectedNumberOfProjects = projectService.getSize() - 1;
        final long expectedNumberOfSessions = sessionService.getSize() - 1;
        userService.removeByLogin("mouse");
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = LoginEmptyException.class)
    public void testRemoveByEmptyLogin() {
        Assert.assertNull(userService.removeByLogin(""));
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByLoginUserNotFound() {
        Assert.assertNull(userService.removeByLogin("dog"));
    }

    @Test
    public void testRemoveByEmail() {
        final long expectedNumberOfEntries = userService.getSize() - 1;
        final long expectedNumberOfTasks = taskService.getSize() - 1;
        final long expectedNumberOfProjects = projectService.getSize() - 1;
        final long expectedNumberOfSessions = sessionService.getSize() - 1;
        userService.removeByEmail("mouse@mouse");
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectService.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionService.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskService.getSize());
    }

    @Test(expected = EmailEmptyException.class)
    public void testRemoveByEmptyEmail() {
        Assert.assertNull(userService.removeByEmail(""));
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveByEmailUserNotFound() {
        Assert.assertNull(userService.removeByEmail("dog@dog"));
    }

    @Test
    public void testSetPassword() {
        @NotNull final String userId = cat.getId();
        @Nullable final String passwordHash = cat.getPasswordHash();
        @NotNull final User user = userService.setPassword(userId, "cat_cat");
        Assert.assertNotEquals(user.getPasswordHash(), passwordHash);
    }

    @Test(expected = IdEmptyException.class)
    public void testSetPasswordEmptyId() {
        userService.setPassword("", "cat");
    }

    @Test(expected = PasswordEmptyException.class)
    public void testSetEmptyPassword() {
        @NotNull final String userId = cat.getId();
        userService.setPassword(userId, "");
    }

    @Test(expected = UserNotFoundException.class)
    public void testSetPasswordUserNotFound() {
        userService.setPassword("some_id", "cat");
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String userId = cat.getId();
        @NotNull final User user = userService.updateUser(userId, "cat", "cat", "cat");
        Assert.assertEquals("cat", user.getLastName());
        Assert.assertEquals("cat", user.getFirstName());
        Assert.assertEquals("cat", user.getMiddleName());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateUserEmptyId() {
        userService.updateUser("", "cat", "cat", "cat");
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateUserNotFound() {
        userService.updateUser("some_id", "cat", "cat", "cat");
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final User user = userService.lockUserByLogin("cat");
        Assert.assertTrue(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testLockUserByEmptyLogin() {
        userService.lockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void testLockUserByLoginNotFound() {
        userService.lockUserByLogin("dog");
    }

    @Test
    public void testUnlockUserByLogin() {
        @NotNull final User user = userService.unlockUserByLogin("mouse");
        Assert.assertFalse(user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void testUnlockUserByEmptyLogin() {
        userService.unlockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void testUnlockUserByLoginNotFound() {
        userService.unlockUserByLogin("dog");
    }

    @Test
    public void testLoginExist() {
        Assert.assertTrue(userService.isLoginExist("cat"));
    }

    @Test
    public void testEmptyLoginExist() {
        Assert.assertFalse(userService.isLoginExist(""));
    }

    @Test
    public void testEmailExist() {
        Assert.assertTrue(userService.isEmailExist("cat@cat"));
    }

    @Test
    public void testEmptyEmailExist() {
        Assert.assertFalse(userService.isEmailExist(""));
    }

    @After
    public void clearRepository() {
        userService.removeAll();
    }

}
